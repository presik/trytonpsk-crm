# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields


class Case(ModelSQL, ModelView):
    "CRM Case"
    __name__ = "crm.case"
    name = fields.Char('Name', required=True, translate=True)
    parent = fields.Many2One('crm.case', 'Parent')
    childs = fields.One2Many('crm.case', 'parent', 'Children')

    @classmethod
    def __setup__(cls):
        super(Case, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    def get_rec_name(self, name):
        if self.parent:
            return self.parent.get_rec_name(name) + ' / ' + self.name
        else:
            return self.name
