# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class CRMChart(metaclass=PoolMeta):
    __name__ = "crm.opportunity"

    @classmethod
    def report_opportunity_by_agent(cls, args):
        fields_names = ['agent.rec_name', 'state', 'amount']
        records = cls.search_read([], fields_names=fields_names)
        res = records
        return res
