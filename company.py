# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    cloudinary_name = fields.Char('Cloudinary Name')
    cloudinary_api_key = fields.Char('Cloudinary Api Key')
    cloudinary_api_secret = fields.Char('Cloudinary Api Secret')
    cloudinary_upload_preset = fields.Char('Cloudinary Upload Preset')
    cloudinary_upload_url = fields.Char('Cloudinary Upload Url')
