
from trytond.pool import PoolMeta


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        # cls.method.selection.extend([
        #         ('crm.opportunity.validation.history|table_query_consult',
        #             "Prueba")
        #         ])
