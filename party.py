# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval

STATES = {
    'readonly': ~Eval('active', True)
}
DEPENDS = ['active']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    # company = fields.Many2One('party.party', 'Company')
    # sales_count = fields.Integer('Sales Count')
    # opportunities = fields.One2Many('crm.opportunity', 'party',
    #     'Opportunities', domain=[('party', '=', Eval('id'))])

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls._buttons.update({
            'add_opportunity': {
                'invisible': False,
            },
        })

    @classmethod
    @ModelView.button_action('sale_opportunity.act_opportunity_form')
    def add_opportunity(cls, parties):
        pass

    def get_opportunities(self, name=None):
        pool = Pool()
        Opportunity = pool.get('crm.opportunity')
        opportunities = Opportunity.search([
            ('party', '=', self.id),
        ])
        opportunities_ids = [o.id for o in opportunities]
        return opportunities_ids
