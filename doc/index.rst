CRM Module
###############

The Customer Relationship Management Module for the bussiness.


CRM
*****

Customer relationship management (CRM) is a widely-implemented strategy for 
managing a company’s interactions with customers, clients and sales prospects.
It involves using technology to organize, automate, and synchronize business 
processes—principally sales activities, but also those for marketing, customer
service, and technical support. The overall goals are to find, attract, and win
new clients, nurture and retain those the company already has, entice former 
clients back into the fold, and reduce the costs of marketing and client service.
Customer relationship management describes a company-wide business strategy
including customer-interface departments as well as other departments.[1]

[1] www.wikipedia.org
