# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction

STATES = {
    'readonly': (Eval('state') != 'draft'),
}


class Activity(Workflow, ModelSQL, ModelView):
    "CRM Activity"
    __name__ = 'crm.activity'
    _rec_name = 'subject'
    _READONLY = {'readonly': Eval('state') == 'done'}
    opportunity = fields.Many2One('crm.opportunity', 'Opportunity',
        required=True, readonly=True)
    subject = fields.Char('Subject', states=_READONLY)
    type_ = fields.Selection([
        ('action', 'Action'),
        ('call', 'Call'),
        ('on_site_appointment', 'On-Site Appointment'),
        ('virtual_appointment', 'Virtual Appointment'),
        ('chat', 'Chat'),
        ('email', 'Email'),
        ], 'Type', depends=['state'], states=_READONLY)
    type_info = fields.Char('Type Info', states=STATES)
    planned_date = fields.Date('Planned Date', states=STATES, required=True)
    planned_time = fields.Time('Planned Time', states=STATES)
    duration = fields.Integer('Duration', help="In minutes", states=STATES)
    effective_date = fields.Date('Effective Date', states=STATES)
    description = fields.Text('Description', states=STATES)
    agent = fields.Many2One('commission.agent', 'Agent', required=False)
    customer = fields.Function(fields.Many2One(
        'crm.prospect', 'Customer', required=False), 'get_customer')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('planned', 'Planned'),
            ('done', 'Done'),
            ('cancelled', 'Cancelled'),
        ], 'State', required=True)
    state_string = state.translated('state')

    @classmethod
    def __setup__(cls):
        super(Activity, cls).__setup__()
        cls._order.insert(0, ('planned_date', 'DESC'))
        cls._order.insert(1, ('id', 'DESC'))
        cls._transitions |= set((
            ('draft', 'pending'),
            ('pending', 'done'),
            ('pending', 'draft'),
            ('pending', 'cancelled'),
            ('cancelled', 'draft'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'done']),
                },
            'cancel': {
                'invisible': Eval('state').in_(['cancelled', 'done']),
                },
            'pending': {
                'invisible': Eval('state') != 'draft',
                },
            'done': {
                'invisible': Eval('state') != 'pending',
                },
            })

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('pending')
    def pending(cls, records):
        for record in records:
            record.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    def get_customer(self, name=None):
        if self.opportunity and self.opportunity.prospect:
            return self.opportunity.prospect.id
        else:
            return self.opportunity.contact
