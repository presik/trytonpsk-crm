# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
"Sales extension for managing leads and opportunities"
from datetime import date, datetime
from decimal import Decimal

from sql import Literal
from sql.aggregate import Count, Max, Sum
from sql.conditionals import Case, Coalesce
from sql.functions import Extract
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.ir.attachment import AttachmentCopyMixin
from trytond.ir.note import NoteCopyMixin
from trytond.model import ModelSQL, ModelView, Workflow, fields, sequence_ordered
from trytond.model.exceptions import AccessError
from trytond.modules.account.tax import TaxableMixin
from trytond.modules.company import CompanyReport
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, Get, If, In
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard

ZERO = Decimal('0.0')


class LeadOrigin(ModelSQL, ModelView):
    "CRM Lead Origin"
    __name__ = 'crm.lead_origin'
    name = fields.Char("Lead Origin")
    time = fields.Integer("time", states={'required': True})


class Prospect(ModelSQL, ModelView):
    "CRM Prospect"
    __name__ = 'crm.prospect'
    name = fields.Char("Name", required=True)
    phone = fields.Char('Contact Phone')
    email = fields.Char('Email')
    agent = fields.Many2One('commission.agent', 'Agent')
    country = fields.Many2One('country.country', 'Country')
    company = fields.Char("Company")


class Opportunity(
        Workflow, ModelSQL, ModelView,
        AttachmentCopyMixin, NoteCopyMixin):
    "CRM Opportunity"
    __name__ = "crm.opportunity"
    _history = True
    _rec_name = 'number'

    _states_opp = {
        'readonly': Eval('state').in_(['converted', 'won', 'cancelled', 'lost']),
    }
    _states_start = {
        'readonly': ~Eval('state').in_(['opportunity', 'converted', 'won', 'lost']),
    }
    _depends_start = ['state']
    _states_stop = {
        'readonly': Eval('state').in_(
            ['cancelled']),
    }
    _depends_stop = ['state']
    number = fields.Char('Number', readonly=True, required=True)
    prospect = fields.Many2One('crm.prospect', 'Prospect', states=_states_opp)
    party_contact = fields.Char('Party Contact', readonly=True)
    contact_phone = fields.Char('Contact Phone', states=_states_opp)
    contact_email = fields.Char('Email')
    reference = fields.Char('Reference', states=_states_opp)
    party = fields.Many2One(
        'party.party', "Party",
        states={
            'readonly': Eval('state').in_(['cancelled', 'converted', 'won', 'lost']),
        },
        context={
            'company': Eval('company', -1),
        },
        depends=['state', 'company'])
    party_category = fields.Many2One('party.category', 'Party Category')
    contact = fields.Many2One(
        'party.contact_mechanism', "Contact",
        domain=[('party', '=', Eval('party'))],
        context={
            'company': Eval('company', -1),
        },
        search_context={
            'related_party': Eval('party'),
        },
        states={
            'readonly': Eval('state').in_(['won', 'lost']),
            # 'required': ~Eval('state').in_(['lead', 'lost', 'cancelled']),
        },
        depends=['party', 'company'])
    address = fields.Many2One('party.address', 'Address',
          domain=[('party', '=', Eval('party'))], depends=['party', 'state'],
          states={
              'readonly': Eval('state').in_(['won', 'lost']),
              # 'required': ~Eval('state').in_(['lead', 'lost', 'cancelled']),
          })
    company = fields.Many2One('company.company', 'Company', required=True,
          states={
              'readonly': _states_stop['readonly'] | Eval('party', True),
          },
          domain=[
              ('id', If(In('company', Eval('context', {})), '=', '!='),
               Get(Eval('context', {}), 'company', 0)),
          ],
          depends=_depends_stop)
    currency = fields.Function(fields.Many2One('currency.currency',
        'Currency'), 'get_currency')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
        'get_currency_digits')
    amount = fields.Numeric('Amount', digits=(16, Eval('currency_digits', 2)),
            states={
                'readonly': ~Eval('state').in_(['opportunity', 'converted', 'won', 'lost']),
                # 'required': ~Eval('state').in_(['lead', 'lost', 'cancelled']),
            }, depends=[*_depends_stop, "currency_digits"],
            help='Estimated revenue amount.')
    payment_term = fields.Many2One('account.invoice.payment_term',
           'Payment Term')
    agent = fields.Many2One('commission.agent', 'Agent',
             states={
                 'readonly': Eval('state').in_(['won', 'lost']),
             },
           depends=['state', 'company'],
           domain=[('company', '=', Eval('company'))])
    start_date = fields.Date('Start Date', required=True,
         states=_states_start, depends=_depends_start)
    end_date = fields.Date('End Date', states=_states_stop,
        depends=_depends_stop)
    description = fields.Char('Description',
          states={
              'readonly': Eval('state').in_(['won', 'lost']),
              'required': ~Eval('state').in_(['lead', 'lost', 'cancelled']),
          })
    comment = fields.Text('Comment', states=_states_stop, depends=_depends_stop)
    lines = fields.One2Many('crm.opportunity.line', 'opportunity', 'Lines',
            states={
                'readonly': Eval('state').in_(['won', 'lost']),
                # 'required': ~Eval('state').in_(['lead', 'lost', 'cancelled']),
            })
    activities = fields.One2Many('crm.activity', 'opportunity', 'Activities',
            states={
                'readonly': Eval('state').in_(['won', 'lost']),
            })
    conversion_probability = fields.Float('Conversion Probability',
          digits=(1, 4), required=True,
          domain=[
              ('conversion_probability', '>=', 0),
              ('conversion_probability', '<=', 1),
          ],
         states={
                  'readonly': Eval('state').in_(['won', 'lost']),
              },
          depends=['state'], help="Percentage between 0 and 100.")
    lost_reason = fields.Text('Reason for loss', states={
        'invisible': Eval('state') != 'lost',
    }, depends=['state'])
    sales = fields.One2Many('sale.sale', 'origin', 'Sales')
    # city = fields.Many2One('party.city_code', 'City')
    city = fields.Many2One('country.city', 'City')
    state = fields.Selection([
       ('lead', 'Lead'),
       ('opportunity', 'Opportunity'),
       ('converted', 'Converted'),
       ('won', 'Won'),
       ('cancelled', 'Cancelled'),
       ('lost', 'Lost'),
    ], "State", required=True, readonly=True)
    type = fields.Selection([
        ('sale', 'Sale'),
        ('contract', 'Contract'),
        ], "Type", required=True,
        states={
            'readonly': Eval('state').in_(['won', 'lost']),
        })
    lead_origin = fields.Many2One('crm.lead_origin', 'Lead Origin',
                                  states={'required': True})
    total_amount = fields.Function(
        fields.Numeric('Total Amount', digits=(16, 2)), 'get_total_amount')
    untaxed_amount = fields.Function(fields.Numeric('Untaxed Amount', digits=(16, 2)),
            'get_untaxed_amount')
    tax_amount = fields.Function(fields.Numeric('Taxes', digits=(16, 2)),
            'get_tax_amount')
    conditions = fields.Many2Many('crm.opportunity-sale.condition',
        'opportunity', 'condition', 'Commercial Conditions')
    cancelled_reason = fields.Many2One(
        'crm.opportunity_cancelled_reason', 'Cancelled Reason Concept',
        states={
            'invisible': ~Eval('state').in_(['won']),
        }, depends=['state'])

    @classmethod
    def __setup__(cls):
        super(Opportunity, cls).__setup__()
        cls._order.insert(0, ('start_date', 'DESC'))
        cls._transitions |= set((
            ('lead', 'opportunity'),
            ('opportunity', 'converted'),
            ('opportunity', 'cancelled'),
            ('cancelled', 'opportunity'),
            ('cancelled', 'converted'),
            ('converted', 'cancelled'),
            ('converted', 'opportunity'),
            ('converted', 'won'),
            ('converted', 'lost'),
            ('won', 'converted'),
            ('lost', 'converted'),
        ))
        cls._buttons.update({
            'opportunity': {
                'invisible': Eval('state').in_(
                    ['won', 'lost', 'opportunity', 'cancelled']),
            },
            'converted': {
                'invisible': Eval('state').in_(
                    ['lead', 'convert', 'cancelled']),
            },
            'won': {
                'invisible': ~Eval('state').in_(['converted']),
            },
            'lost': {
                'invisible': ~Eval('state').in_(['converted']),
            },
             'cancelled': {
                'invisible': Eval('state').in_([
                    'lead', 'won', 'lost', 'cancelled']),
            },
        })

    # @fields.depends('party')
    # def get_is_prospect(self, name=None):
    #     res = True
    #     if self.party:
    #         Invoice = Pool().get('account.invoice')
    #         invoices = Invoice.search([
    #                 ('party','=',self.party.id),
    #                 ('type','=','out'),
    #                 ('state','!=','draft')
    #             ])
    #         if len(invoices) > 0:
    #             res = False
    #     return res

    # def get_is_approved(self, name=None):
    #     res = True
    #     if self.party_validations:
    #         for validation in self.party_validations:
    #             if (validation.response !='approved'):
    #                 res = False
    #     return res
    @fields.depends('prospect', 'contact_email', 'contact_phone', 'agent')
    def on_change_prospect(self):
        if self.prospect:
            self.contact_phone = self.prospect.phone
            self.contact_email = self.prospect.email
            self.agent = self.prospect.agent

    @staticmethod
    def default_state():
        return 'lead'

    @staticmethod
    def default_start_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_conversion_probability():
        return 0.5

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_agent():
        Agent = Pool().get('commission.agent')
        agents = Agent.search([
            ('user', '=', Transaction().user),
        ])
        if agents:
            return agents[0].id

    @classmethod
    def default_payment_term(cls):
        PaymentTerm = Pool().get('account.invoice.payment_term')
        payment_terms = PaymentTerm.search(cls.payment_term.domain)
        if len(payment_terms) == 1:
            return payment_terms[0].id

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('/tree', 'visual', If(Eval('state') == 'cancelled', 'muted', '')),
        ]

    @classmethod
    def get_resources_to_copy(cls, name):
        return {
            'sale.sale',
        }

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        # history = []
        Config = pool.get('crm.configuration')
        config = Config().get_configuration()
        vlist = [x.copy() for x in vlist]
        # default_company = cls.default_company()
        for vals in vlist:
            if vals.get('number') is None and config:
                vals['number'] = config.opportunity_sequence.get()

        opportunity = super(Opportunity, cls).create(vlist)
        # print(opportunity,'oportunidad')
        # value = {
        #     'opportunity':opportunity[id],
        #     'validated_by':Transaction().user,
        #     'create_date':date.today(),
        #     'action':'Creación de la oportunidad'
        # }
        # history.append(value)
        # history = traceability.create(history)
        return opportunity

    @classmethod
    def copy(cls, opportunities, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('number', None)
        default.setdefault('sales', None)
        default.setdefault('contracts', None)
        default.setdefault('converted_by')
        return super(Opportunity, cls).copy(opportunities, default=default)

    def get_currency(self, name):
        return self.company.currency.id

    def get_currency_digits(self, name):
        return self.company.currency.digits

    @fields.depends('company')
    def on_change_company(self):
        if self.company:
            self.currency = self.company.currency
            self.currency_digits = self.company.currency.digits

    @fields.depends('party')
    def on_change_party(self):
        if self.party and self.party.customer_payment_term:
            self.payment_term = self.party.customer_payment_term
        else:
            self.payment_term = self.default_payment_term()

    def _get_contract_opportunity(self):
        """
        Return contract for an opportunity
        """
        # Contract = Pool().get('sale.contract')
        # print(Contract)
        Date = Pool().get('ir.date')
        today = Date.today()
        return {
            'description': self.description,
            'party': self.party,
            # 'salesman':self.employee,
            'payment_term': self.payment_term,
            'company': self.company,
            'currency': self.company.currency,
            'comment': self.comment,
            'reference': self.reference,
            'contract_date': today,
            'origin': self,
            'state': 'draft',
        }

    def _get_sale_opportunity(self):
        """
        Return sale for an opportunity
        """
        Sale = Pool().get('sale.sale')
        sale = Sale(
            description=self.description,
            party=self.party,
            payment_term=self.payment_term,
            company=self.company,
            agent=self.agent,
            # salesman=self.employee,
            invoice_address=self.address,
            shipment_address=self.address,
            reference=self.reference,
            currency=self.company.currency,
            comment=self.comment,
            sale_date=None,
            origin=self,
            warehouse=Sale.default_warehouse(),
        )
        sale.on_change_party()
        return sale

    def create_sale(self):
        """
        Create a sale for the opportunity and return the sale
        """
        sale = self._get_sale_opportunity()
        sale_lines = []
        if self.lines:
            for line in self.lines:
                sale_lines.append(line.get_sale_line(sale))
            sale.lines = sale_lines
            return sale
        else:
            raise UserError("No hay lineas de producto")

    def create_contract(self):
        contract = self._get_contract_opportunity()
        # Contract = Pool().get('sale.contract')
        # contract = opportunity.create_contract()
        # Contract.save([contract])
        contract_lines = []
        if self.lines:
            for line in self.lines:
                contract_lines.append(line.get_contract_line())
            contract['product_lines'] = [('create', contract_lines)]
            return contract
        else:
            raise UserError("No hay lineas de producto")

    @classmethod
    def delete(cls, opportunities):
        # Cancel before delete
        cls.cancel(opportunities)
        for opportunity in opportunities:
            if opportunity.state != 'cancelled':
                raise AccessError(
                    gettext('crm.msg_opportunity_delete_cancel',
                            opportunity=opportunity.rec_name))
        super(Opportunity, cls).delete(opportunities)

    @classmethod
    @ModelView.button
    @Workflow.transition('opportunity')
    def opportunity(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('converted')
    def converted(cls, records):
        cls.procces_opportunity(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('won')
    def won(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('quote_approbation')
    def quote_approbation(cls, records):
        pass

    # @classmethod
    # @ModelView.button
    # @Workflow.transition('quotation')
    # @set_employee('converted_by')
    # def quotation(cls, records):
    #     # Analyze if party validation is true in all lines
    #     cls.get_party_validation(records)
    #     # cls.procces_opportunity(records)
    #     history = []
    #     for  record in records:
    #         value = {
    #             'opportunity':record.id,
    #             'create_date':date.today(),
    #             'action':'Cambio de estado a Cotización'
    #         }
    #         history.append(value)

    @classmethod
    def send_email(cls, opportunity_id, name='opportunity_email'):
        pool = Pool()
        config = pool.get('crm.configuration')(1)
        oppo = cls(opportunity_id)
        Template = pool.get('email.template')
        # Activity = pool.get('email.activity')
        email = oppo.contact_email
        if oppo.party and oppo.party.email:
            email = oppo.party.email

        if not email:
            print("Correo no enviado no encontre un correo valido...!")
            return

        template = getattr(config, name)
        if not template:
            return
        template.subject = f'{template.subject} No. {oppo.number}'
        if email:
            Template.send(template, oppo, email, attach=True)
            # Activity.create([{
            #     'template': template.id,
            #     'origin': str(oppo),
            #     'status': 'sended',
            # }])
        else:
            raise UserError(gettext('hotel.msg_missing_party_email'))

    @property
    def is_forecast(self):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        return self.end_date or datetime.date.max > today

    @classmethod
    def procces_opportunity(cls, records):
        pool = Pool()
        Sale = pool.get('sale.sale')
        for opportunity in records:
            if opportunity.type == 'contract':
                Contract = pool.get('sale.contract')
                contract = opportunity.create_contract()
                Contract.create([contract])
            else:
                sales = [o.create_sale() for o in records if not o.sales]
                Sale.save(sales)
                for sale in sales:
                    sale.origin.copy_resources_to(sale)
                Sale.set_number(sales)
                # sale = opportunity.create_sale()
                # Sale.save(sale)

    @classmethod
    @ModelView.button
    @Workflow.transition('lost')
    def lost(cls, records):
        Date = Pool().get('ir.date')
        cls.write([o for o in records], {
            'end_date': Date.today(),
            'state': 'lost',
        })
        history = []
        for record in records:
            value = {
                'opportunity': record.id,
                'validated_by': Transaction().user,
                'create_date': date.today(),
                'action': 'Cambio de estado a Perdido',
            }
            history.append(value)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancelled(cls, records):
        Date = Pool().get('ir.date')
        cls.write([o for o in records if o.cancelled_reason], {
            'end_date': Date.today(),
        })
        history = []
        for record in records:
            value = {
                'opportunity': record.id,
                'validated_by': Transaction().user,
                'create_date': date.today(),
                'action': 'Cambio de estado a Anulado',
            }
            history.append(value)

    @staticmethod
    def _sale_won_states():
        return ['confirmed', 'processing', 'done']

    @staticmethod
    def _sale_lost_states():
        return ['cancelled']

    def is_won(self):
        sale_won_states = self._sale_won_states()
        sale_lost_states = self._sale_lost_states()
        end_states = sale_won_states + sale_lost_states
        return (self.sales
                and all(s.state in end_states for s in self.sales)
                and any(s.state in sale_won_states for s in self.sales))

    def is_lost(self):
        sale_lost_states = self._sale_lost_states()
        return (self.sales
                and all(s.state in sale_lost_states for s in self.sales))

    @property
    def sale_amount(self):
        pool = Pool()
        Currency = pool.get('currency.currency')

        if not self.sales:
            return

        sale_lost_states = self._sale_lost_states()
        amount = 0
        for sale in self.sales:
            if sale.state not in sale_lost_states:
                amount += Currency.compute(sale.currency, sale.untaxed_amount,
                                           self.currency)
        return amount

    @classmethod
    def process(cls, opportunities):
        won = []
        lost = []
        converted = []
        for opportunity in opportunities:
            sale_amount = opportunity.sale_amount
            if opportunity.amount != sale_amount:
                opportunity.amount = sale_amount
            if opportunity.is_won():
                won.append(opportunity)
            elif opportunity.is_lost():
                lost.append(opportunity)
            elif (opportunity.state in ('opportunity', 'lead')
                    and opportunity.sales):
                converted.append(opportunity)
        cls.save(opportunities)
        if won:
            cls.won(won)
        if lost:
            cls.lost(lost)
        if converted:
            cls.convert(converted)

    def get_total_amount(self, name):
        return self.untaxed_amount + self.tax_amount

    def get_untaxed_amount(self, name):
        return sum(line.amount for line in self.lines)

    def get_tax_amount(self, name):
        taxes_computed = Opportunity._get_taxes(self.lines)
        res = sum([t['amount'] for t in taxes_computed], ZERO)
        return self.currency.round(res)

    @classmethod
    def _get_taxes(cls, lines):
        taxes = []
        today = Pool().get("ir.date").today()
        with Transaction().set_context({}):
            for line in lines:
                currency = line.opportunity.currency
                _taxes = line.product.customer_taxes_used
                if line.unit_price and line.quantity and _taxes:
                    taxes.extend(cls.compute_taxes(
                        _taxes,
                        line.unit_price,
                        line.quantity,
                        line.start_invoice_date or today,
                        currency),
                    )

        _taxes = {}
        for ctax in taxes:
            if ctax['tax'] not in _taxes:
                _taxes[ctax['tax']] = ctax
            else:
                _taxes[ctax['tax']]['base'] += ctax['base']
                _taxes[ctax['tax']]['amount'] += ctax['amount']
        return _taxes.values()

    @classmethod
    def compute_taxes(cls, _taxes, unit_price, quantity, date, currency=None):
        Tax = Pool().get('account.tax')
        l_taxes = Tax.compute(list(_taxes), unit_price, quantity, date)
        taxes = []
        for tax in l_taxes:
            taxline = TaxableMixin._compute_tax_line(**tax)
            # Base must always be rounded per folio as there will
            # be one tax folio per taxable_lines
            if currency:
                taxline['base'] = currency.round(taxline['base'])
            taxes.append(taxline)
        return taxes

# class OpportunityKind(ModelSQL, ModelView):
#     ''' Opportunity Kind Concept '''
#     'Opportunity Kind Concept'
#     __name__ = 'crm.opportunity_kind_concept'
#     name = fields.Char('Concept Name', required=True)


class OpportunityCancellReason(ModelSQL, ModelView):
    """ Opportunity Cancell Reason """
    'Opportunity Cancelled Reason'
    __name__ = 'crm.opportunity_cancelled_reason'
    name = fields.Char('Reason', required=True)

# class OpportunityKind(ModelSQL, ModelView):
#     ''' Opportunity Kind'''
#     __name__ = 'crm.opportunity.kind'
#     concept = fields.Many2One('crm.opportunity_kind_concept', 'Concept Opportunity Kind', required=True )
#     opportunity = fields.Many2One('crm.opportunity', 'Opportunity')


# class CrmOpportunityFollowUp(sequence_ordered(), ModelSQL, ModelView):
#     'CRM Opportunity FollowUp'
#     __name__ = "crm.opportunity.follow_up"
#     opportunity = fields.Many2One('crm.opportunity', 'Opportunity',
#         required=True)
#     follow_date = fields.Date('Follow Date')
#     action = fields.Char('Action')
#     notes = fields.Text('Notes')
#     done_by = fields.Many2One('res.user', 'Done By')
#     state = fields.Selection([
#         ('draft', "Draft"),
#         ('done', "Done"),
#     ], "State", required=False, readonly=True)
#
#     @staticmethod
#     def default_state():
#         return 'draft'


class CrmOpportunityLine(sequence_ordered(), ModelSQL, ModelView):
    "CRM Opportunity Line"
    __name__ = "crm.opportunity.line"
    _history = True
    _states = {
        'readonly': Eval('opportunity_state').in_([
            'won',
            'lost',
            'cancelled']),
    }
    _depends = ['opportunity_state']
    billing_frecuency = fields.Selection([
        ('one', "One"),
        ('monthly', "Monthly"),
        ('bimonthly', "Bimonthly"),
        ('biannual', "Biannual"),
        ('annual', "Annual")], "Billing Frecuency",
        states={
            'readonly': Eval('state').in_(['won', 'lost']),
        })
    opportunity = fields.Many2One('crm.opportunity', 'Opportunity',
          ondelete='CASCADE', required=True, depends=_depends,
          states={
              'readonly': _states['readonly'] & Bool(Eval('opportunity')),
          })
    opportunity_state = fields.Function(
        fields.Selection('get_opportunity_states', "Opportunity State"),
        'on_change_with_opportunity_state')
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[('salable', '=', True)], states=_states, depends=_depends)
    quantity = fields.Float('Quantity', required=True,
        digits=(16, Eval('unit_digits', 2)),
        states=_states, depends=['unit_digits'] + _depends)
    unit = fields.Many2One('product.uom', 'Unit', required=True,
        states=_states, depends=_depends)
    unit_digits = fields.Function(
        fields.Integer('Unit Digits'), 'on_change_with_unit_digits')
    taxes_amount = fields.Function(
        fields.Numeric('Tax Line'), 'get_taxes_amount')
    amount = fields.Function(
        fields.Numeric('Amount', digits=(16, 2)), 'get_amount')
    description = fields.Text('Description')
    unit_price = fields.Numeric('Unit Price', digits=(16, 2),
                                states={'required': True})
    start_invoice_date = fields.Date('Start Invoice Date')
    amount_taxed = fields.Function(
        fields.Numeric('Amount Taxed', digits=(16, 2)), 'get_amount_taxed')
    payment_term = fields.Many2One(
        'account.invoice.payment_term', 'Payment Term')
    availability = fields.Char('Availability')

    del _states, _depends

    # @classmethod
    # def __register__(cls, module_name):
    #     super().__register__(module_name)
    #     table_h = cls.__table_handler__(module_name)
    #     table_h.drop_column('total_line')

    @staticmethod
    def default_billing_frecuency():
        return 'one'

    @staticmethod
    def default_quantity():
        return 1

    @classmethod
    def get_opportunity_states(cls):
        pool = Pool()
        Opportunity = pool.get('crm.opportunity')
        return Opportunity.fields_get(['state'])['state']['selection']

    @fields.depends('opportunity', '_parent_opportunity.state')
    def on_change_with_opportunity_state(self, name=None):
        if self.opportunity:
            return self.opportunity.state

    def _get_taxes(self, product, unit_price, quantity, currency=None):
        # Return tax amount for: product, unit_price, quantity
        Tax = Pool().get('account.tax')
        Date = Pool().get('ir.date')
        date = self.start_invoice_date or Date.today()
        res = 0
        _taxes = list(product.customer_taxes_used)
        list_taxes = Tax.compute(_taxes, unit_price, quantity, date)
        for tax in list_taxes:
            taxline = TaxableMixin._compute_tax_line(**tax)
            res += taxline['amount']
        return res

    @fields.depends('product')
    def get_taxes_amount(self, name=None):
        res = 0
        if all(self.product, self.unit_price, self.quantity):
            res = self._get_taxes(self.product, self.unit_price, self.quantity)
        return res

    @fields.depends('product')
    def get_amount(self, name=None):
        res = 0
        if self.unit_price and self.quantity:
            res = float(self.unit_price) * float(self.quantity)
            res = self.opportunity.currency.round(Decimal(res))
        return res

    @fields.depends('product')
    def get_amount_taxed(self, name=None):
        res = 0
        if self.amount:
            res = float(self.amount) + self.tax
        return res

    @fields.depends('unit')
    def on_change_with_unit_digits(self, name=None):
        if self.unit:
            return self.unit.digits
        return 2

    @fields.depends('product')
    def on_change_with_unit_price(self, name=None):
        if self.product:
            return self.product.list_price

    @fields.depends('product', 'unit', 'description')
    def on_change_product(self):
        if not self.product:
            return

        category = self.product.sale_uom.category
        self.description = self.product.name
        if not self.unit or self.unit.category != category:
            self.unit = self.product.sale_uom
            self.unit_digits = self.product.sale_uom.digits

    def get_sale_line(self, sale):
        """
        Return sale line for opportunity line
        """
        SaleLine = Pool().get('sale.line')
        sale_line = SaleLine(
            type='line',
            product=self.product,
            sale=sale,
            description=None,
        )
        # if self.opportunity.employee:
        #     sale_line.operation_center = self.opportunity.employee.operation_center

        sale_line.on_change_product()
        self._set_sale_line_quantity(sale_line)
        sale_line.on_change_quantity()
        sale_line.description = self.description
        sale_line.unit_price = self.unit_price
        # sale_line.base_price = self.unit_price
        return sale_line

    def get_contract_line(self):
        """
        Return contract line for opportunity line
        """
        # ContractLine = Pool().get('sale.contract.product_line')
        contract_line = {
            'type': 'line',
            # 'contract': contract,
            'product': self.product,
            'description': self.product.name,
            'unit_price': self.unit_price,
            # quantity = self.quantity,
        }
        # print(contract, "Contract in contract line")
        # print(contract_line, "contract line")
        return contract_line

    def _set_sale_line_quantity(self, sale_line):
        sale_line.quantity = self.quantity
        sale_line.unit = self.unit

    def get_rec_name(self, name):
        pool = Pool()
        Lang = pool.get('ir.lang')
        lang = Lang.get()
        return (lang.format(
                '%.*f', (self.unit.digits, self.quantity or 0))
                + '%s %s @ %s' % (
                self.unit.symbol, self.product.rec_name,
                self.opportunity.rec_name))

    @classmethod
    def search_rec_name(cls, name, clause):
        return [('product.rec_name',) + tuple(clause[1:])]


class SaleOpportunityReportMixin:
    __slots__ = ()
    number = fields.Integer('Number')
    converted = fields.Integer('Converted')
    conversion_rate = fields.Function(fields.Float('Conversion Rate',
                                                   digits=(1, 4)), 'get_conversion_rate')
    won = fields.Integer('Won')
    winning_rate = fields.Function(fields.Float('Winning Rate', digits=(1, 4)),
                                   'get_winning_rate')
    lost = fields.Integer('Lost')
    company = fields.Many2One('company.company', 'Company')
    currency = fields.Function(fields.Many2One('currency.currency',
            'Currency'), 'get_currency')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
    amount = fields.Numeric('Amount', digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'])
    converted_amount = fields.Numeric('Converted Amount',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'])
    conversion_amount_rate = fields.Function(fields.Float(
        'Conversion Amount Rate', digits=(1, 4)), 'get_conversion_amount_rate')
    won_amount = fields.Numeric('Won Amount',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    winning_amount_rate = fields.Function(fields.Float(
        'Winning Amount Rate', digits=(1, 4)), 'get_winning_amount_rate')

    @staticmethod
    def _converted_state():
        return ['converted', 'won']

    @staticmethod
    def _won_state():
        return ['won']

    @staticmethod
    def _lost_state():
        return ['lost']

    def get_conversion_rate(self, name):
        if self.number:
            digits = getattr(self.__class__, name).digits[1]
            return round(float(self.converted) / self.number, digits)
        else:
            return 0.0

    def get_winning_rate(self, name):
        if self.number:
            digits = getattr(self.__class__, name).digits[1]
            return round(float(self.won) / self.number, digits)
        else:
            return 0.0

    def get_currency(self, name):
        return self.company.currency.id

    def get_currency_digits(self, name):
        return self.company.currency.digits

    def get_conversion_amount_rate(self, name):
        if self.amount:
            digits = getattr(self.__class__, name).digits[1]
            return round(
                float(self.converted_amount) / float(self.amount), digits)
        else:
            return 0.0

    def get_winning_amount_rate(self, name):
        if self.amount:
            digits = getattr(self.__class__, name).digits[1]
            return round(float(self.won_amount) / float(self.amount), digits)
        else:
            return 0.0

    @classmethod
    def table_query(cls):
        Opportunity = Pool().get('crm.opportunity')
        opportunity = Opportunity.__table__()
        return opportunity.select(
            Max(opportunity.create_uid).as_('create_uid'),
            Max(opportunity.create_date).as_('create_date'),
            Max(opportunity.write_uid).as_('write_uid'),
            Max(opportunity.write_date).as_('write_date'),
            opportunity.company,
            Count(Literal(1)).as_('number'),
            Sum(Case(
                    (opportunity.state.in_(cls._converted_state()),
                        Literal(1)), else_=Literal(0))).as_('converted'),
            Sum(Case(
                    (opportunity.state.in_(cls._won_state()),
                        Literal(1)), else_=Literal(0))).as_('won'),
            Sum(Case(
                    (opportunity.state.in_(cls._lost_state()),
                        Literal(1)), else_=Literal(0))).as_('lost'),
            Sum(opportunity.amount).as_('amount'),
            Sum(Case(
                    (opportunity.state.in_(cls._converted_state()),
                        opportunity.amount),
                else_=Literal(0))).as_('converted_amount'),
            Sum(Case(
                    (opportunity.state.in_(cls._won_state()),
                        opportunity.amount),
                else_=Literal(0))).as_('won_amount'))


class SaleOpportunityEmployee(SaleOpportunityReportMixin, ModelSQL, ModelView):
    "Sale Opportunity per Employee"
    __name__ = 'crm.opportunity_employee'
    employee = fields.Many2One('company.employee', 'Employee')

    @classmethod
    def table_query(cls):
        query = super(SaleOpportunityEmployee, cls).table_query()
        opportunity, = query.from_
        query.columns += (
            Coalesce(opportunity.employee, 0).as_('id'),
            opportunity.employee,
        )
        where = Literal(True)
        if Transaction().context.get('start_date'):
            where &= (opportunity.start_date
                      >= Transaction().context['start_date'])
        if Transaction().context.get('end_date'):
            where &= (opportunity.start_date
                      <= Transaction().context['end_date'])
        query.where = where
        query.group_by = (opportunity.employee, opportunity.company)
        return query


class SaleOpportunityEmployeeContext(ModelView):
    "Sale Opportunity per Employee Context"
    __name__ = 'crm.opportunity_employee.context'
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')


class SaleOpportunityMonthly(SaleOpportunityReportMixin, ModelSQL, ModelView):
    "Sale Opportunity per Month"
    __name__ = 'crm.opportunity_monthly'
    year = fields.Char('Year')
    month = fields.Many2One('ir.calendar.month', "Month")
    year_month = fields.Function(fields.Char('Year-Month'),
                                 'get_year_month')

    @classmethod
    def __setup__(cls):
        super(SaleOpportunityMonthly, cls).__setup__()
        cls._order.insert(0, ('year', 'DESC'))
        cls._order.insert(1, ('month.index', 'DESC'))

    def get_year_month(self, name):
        return '%s-%s' % (self.year, self.month.index)

    @classmethod
    def table_query(cls):
        pool = Pool()
        Month = pool.get('ir.calendar.month')
        month = Month.__table__()
        query = super(SaleOpportunityMonthly, cls).table_query()
        opportunity, = query.from_
        type_id = cls.id.sql_type().base
        type_year = cls.year.sql_type().base
        year_column = Extract('YEAR', opportunity.start_date,
                              ).cast(type_year).as_('year')
        month_index = Extract('MONTH', opportunity.start_date)
        query.from_ = opportunity.join(
            month, condition=month_index == month.index)
        query.columns += (
            Max(Extract('MONTH', opportunity.start_date)
                + Extract('YEAR', opportunity.start_date) * 100,
                ).cast(type_id).as_('id'),
            year_column,
            month.id.as_('month'))
        query.group_by = (year_column, month.id, opportunity.company)
        return query


class OpportunityReport(CompanyReport):
    __name__ = 'crm.opportunity'


class OpportunityOnlyReport(CompanyReport):
    __name__ = 'crm.opportunity_only'


class OpportunityWithoutTaxReport(CompanyReport):
    __name__ = 'crm.opportunity_without_tax'


class OpportunityLargeReport(CompanyReport):
    __name__ = 'crm.opportunity_large_format'


class SaleOpportunityEmployeeMonthly(
        SaleOpportunityReportMixin, ModelSQL, ModelView):
    "Sale Opportunity per Employee per Month"
    __name__ = 'crm.opportunity_employee_monthly'
    year = fields.Char('Year')
    month = fields.Many2One('ir.calendar.month', "Month")
    employee = fields.Many2One('company.employee', 'Employee')

    @classmethod
    def __setup__(cls):
        super(SaleOpportunityEmployeeMonthly, cls).__setup__()
        cls._order.insert(0, ('year', 'DESC'))
        cls._order.insert(1, ('month.index', 'DESC'))
        cls._order.insert(2, ('employee', 'ASC'))

    @classmethod
    def table_query(cls):
        pool = Pool()
        Month = pool.get('ir.calendar.month')
        month = Month.__table__()
        query = super(SaleOpportunityEmployeeMonthly, cls).table_query()
        opportunity, = query.from_
        type_id = cls.id.sql_type().base
        type_year = cls.year.sql_type().base
        year_column = Extract('YEAR', opportunity.start_date,
                              ).cast(type_year).as_('year')
        month_index = Extract('MONTH', opportunity.start_date)
        query.from_ = opportunity.join(
            month, condition=month_index == month.index)
        query.columns += (
            Max(Extract('MONTH', opportunity.start_date)
                + Extract('YEAR', opportunity.start_date) * 100
                + Coalesce(opportunity.employee, 0) * 1000000,
                ).cast(type_id).as_('id'),
            year_column,
            month.id.as_('month'),
            opportunity.employee,
        )
        query.group_by = (year_column, month.id,
                          opportunity.employee, opportunity.company)
        return query


class OpportunitySaleConditions(ModelSQL, ModelView):
    "Opportunity Sale Contitions"
    __name__ = 'crm.opportunity-sale.condition'
    opportunity = fields.Many2One('crm.opportunity', 'Opportunity',
        ondelete='CASCADE')
    condition = fields.Many2One('sale.condition', 'Condition',
        ondelete='RESTRICT')


class SummaryOpportunityStart(ModelView):
    "Summary Opportunity Start"
    __name__ = 'crm.summary.opportunity.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True)
    month = fields.Many2One('account.period', 'Month',
        depends=['fiscalyear'], required=True, domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ])
    by_agent = fields.Boolean('By Agent')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), test_state=False).id


class SummaryOpportunity(Wizard):
    "Summary Opportunity"
    __name__ = 'crm.summary.opportunity'
    start = StateView(
        'crm.summary.opportunity.start',
        'crm.summary_opportunity_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('crm.summary_opportunity.report')

    def do_print_(self, action):

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'month': self.start.month.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class SummaryOpportunityReport(Report):
    "Summary Opportunity Report"
    __name__ = 'crm.summary_opportunity.report'

    @classmethod
    def get_country(cls, opportunity):
        country = ''
        if opportunity.prospect:
            country = opportunity.prospect.country.name if opportunity.prospect.country else ''
        elif opportunity.party:
            country = opportunity.party.born_country.name if opportunity.party.born_country else ''
        return country

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Opportunity = pool.get('crm.opportunity')
        Company = pool.get('company.company')
        Period = pool.get('account.period')
        period = Period(data['month'])
        start = period.start_date
        end = period.end_date
        opportunity_dom = ['OR',
                           [('end_date', '>=', start),
                            ('end_date', '<=', end),
                            ('state', 'in', ['converted', 'won'])],
                           [('start_date', '>=', start),
                            ('start_date', '<=', end),
                            ('state', 'not in', ['converted', 'won'])]]
        agents = {}
        general_data = {
            'nationalities': {},
            'lead_origins': {},
        }
        opportunities = Opportunity.search(opportunity_dom)
        for opportunity in opportunities:
            country = cls.get_country(opportunity)
            lead_origin = opportunity.lead_origin and opportunity.lead_origin.name
            cls.get_general_data(opportunity, general_data, 'nationalities', country)
            cls.get_general_data(opportunity, general_data, 'lead_origins', lead_origin)
            agent = opportunity.agent.rec_name
            management_time_exist = True if hasattr(opportunity, 'management_time') else False
            cls.check_agent_exist(agent, agents)
            cls.set_products_in_agent(opportunity, agent, agents)
            cls.get_agent_data(opportunity, agent, agents, 'nationalities', country, management_time_exist)
            cls.get_agent_data(opportunity, agent, agents, 'lead_origins', lead_origin, management_time_exist)
            agents[agent][opportunity.state] += 1
            agents[agent]['total'] += 1
        report_context['records'] = agents.values()
        report_context['general_data'] = general_data
        report_context['start'] = start
        report_context['end'] = end
        report_context['company'] = Company(data['company'])
        return report_context

    @classmethod
    def get_general_data(cls, opportunity, general_data, parameter, key):
        if key in general_data[parameter]:
            general_data[parameter][key]['quantity'] += 1
            if opportunity.state in general_data[parameter][key]:
                general_data[parameter][key][opportunity.state] += 1
        else:
            general_data[parameter].update({
                key: {
                    'name': key,
                    'quantity': 1,
                    'lead': 0,
                    'opportunity': 0,
                    'converted': 0,
                    'won': 0,
                    'cancelled': 0,
                    'lost': 0,
                },
            })
            general_data[parameter][key].update([(opportunity.state, 1)])

    @classmethod
    def check_agent_exist(cls, agent, agents):
        if agent not in agents:
            agents[agent] = {
                        'name': agent,
                        'products': {},
                        'nationalities': {},
                        'lead_origins': {},
                        'total': 0,
                        'lead': 0,
                        'opportunity': 0,
                        'won': 0,
                        'converted': 0,
                        'cancelled': 0,
                        'lost': 0,
                        'management_time': 0,
                    }

    @classmethod
    def set_products_in_agent(cls, opportunity, agent, agents):
        if opportunity.state in ['won', 'converted']:
            for line in opportunity.lines:
                product = line.product.name
                try:
                    agents[agent]['products'][product]['quantity'] += 1
                except KeyError:
                    agents[agent]['products'][product] = {
                        'product': product,
                        'quantity': 1,
                    }

    @classmethod
    def get_agent_data(cls, opportunity, agent, agents,
                       parameter, key, management_time_exist):
        agents_dict = agents[agent][parameter]
        if key in agents_dict:
            agents_dict[key]['quantity'] += 1
            agents_dict[key]['management_time'] += opportunity.management_time if management_time_exist else 0
        else:
            agents_dict.update(
                {key: {
                    'name': key,
                    'quantity': 1,
                    'management_time': 0,
                    'time': 0,
                }},
            )
            if opportunity.lead_origin and opportunity.lead_origin.time:
                agents_dict[key]['time'] = opportunity.lead_origin.time
