# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pyson import Id
from trytond.pyson import Eval


class Configuration(ModelSQL, ModelView):
    'CRM Configuration'
    __name__ = 'crm.configuration'
    company = fields.Many2One('company.company', 'Company', required=True)
    customer_service_sequence = fields.Many2One('ir.sequence',
        'Customer Service Sequence', required=True,
        domain=[('sequence_type', '=', Id('crm', 'sequence_type_crm'))])
    opportunity_email = fields.Many2One('email.template', 'Opportunity Email')
    survey_sequence = fields.Many2One('ir.sequence',
        'Survey Sequence', required=True,
        domain=[('sequence_type', '=',
                Id('crm', 'sequence_type_survey_service'))])
    response_time_customer = fields.Integer('Response Time Customer',
        required=True, help="Limit in hours for response to customer")
    # activity_sequence = fields.Many2One('ir.sequence',
    #     'Activity Sequence', required=True)
    # domain=[('sequence_type', '=', Id('crm', 'sequence_type_activity'))])
    opportunity_sequence = fields.Many2One(
        'ir.sequence', "Opportunity Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=',
                Id('crm', 'sequence_type_crm_opportunity')),
        ])
    efficacy_limit = fields.Integer('Efficacy Limit', help="In Hours")

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def get_configuration(cls):
        res = cls.search([
            ('company', '=', Transaction().context.get('company'))
        ])
        return res[0]
